﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetExternalFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            ExternalFileWrapper2 wr = new ExternalFileWrapper2();

            wr.Test();

            foreach (var item in wr.Files)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}
