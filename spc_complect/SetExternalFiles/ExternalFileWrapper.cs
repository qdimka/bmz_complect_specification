﻿using KompasAPI7;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SetExternalFiles
{
    //Сallback
    [UnmanagedFunctionPointer( CallingConvention.StdCall)]
    public delegate string CallBackExternalFilesHandling(int externalFileType, string externalFileName);

    //Функция
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int ksSetExternalFilesHandlingParam(ExternalFilesHandlingParam param);


    [StructLayout(LayoutKind.Sequential)]
    public struct ExternalFilesHandlingParam
    {
        public CallBackExternalFilesHandling callBack; // указатель на функцию обратной связи CallBackExternalFilesNames
        public int allLevels;     // 1 - по всем уровням, 0 по первому уровню( не спуcкаться в подсборки )
        public bool docChanged;   // Признак изменения документов
        public ExternalFilesHandlingParam(CallBackExternalFilesHandling _callBack = null, int _allLevels = 0, bool _docChanged = false)
        {
            callBack = _callBack;
            allLevels = _allLevels;
            docChanged = _docChanged;
        }
    }

    public enum KDocExternalFileType
    {
        eft_documentFile = 0, // файл документа
        eft_curveStyleLib = 1, // библиотека стилей кривых
        eft_textStyleLib = 2, // библиотека стилей текстов
        eft_hatchStyleLib = 3, // библиотека стилей штриховок
        eft_attrTypesLib = 4, // библиотека типов атрибутов
        eft_layoutLib = 5, // библиотека оформлений
        eft_externalFrgFile = 6, // внешний файл фрагмента
        eft_libraryOfFragments = 7, // библиотека фрагментов
        eft_externalAssemblageFile = 8, // листы сборки, прикрепленные к спецификации
        etf_systemFile = 9, // системные файлы
        eft_libraryOfIngots = 10,// Библиотеки типовых элементов (3D)
        eft_externalPartFile = 11,// внешний файл детали (3D)
        eft_externalRasterFile = 12,// внешние файлы, содержащие растровые изображения
        eft_externalSpcobjDoc = 13,// Документы, подключенные к объектам спецификации
        eft_externalGrdocSpc = 14,// Спецификации, подключенные к листу сборки
        eft_externalDetalDocs = 15,// Документ подключен к объектам спецификации в документах
        eft_externalAssemblyFile = 16,// внешний файл сборки (3D)
        eft_libraryOfModels = 17,// библиотека моделей
        eft_temporaryFile = 18,// временные рабочие файлы
        eft_fileLinkForVar = 19,// файлы-источники для переменных
        eft_fileSatellite = 20,// файл-сателит для слежением за освобождением редактируемого файла
        eft_externalCdwFile = 21,// внешний чертеж
        eft_hyperLink = 22,// гиперссылка
        eft_externalDatafile = 23,// файлы данных
        eft_externalReadingRegime = 24,// файлы режима чтения компонента
        eft_externalAssRprtTable = 25,// Файлы ассоциативных таблиц отчета
        eft_externalHyperText = 26,// Файлы внешних текстовых связок"
        eft_copyExternalGeometry = 27,// Файл-источник копии внешней геометрии
        eft_externalBilletPart = 28,// Файлы деталей заготовок
        eft_externalLayoutGeometry = 29 // Файлы компоновочной геометрии
    };

    //public class ExternalFileWrapper
    //{
    //    private IApplication kompas7;
    //    private string progId7 = "KOMPAS.Application.7";
    //    private static string to = @"C:\Users\qdimk\Desktop\Комплектование";
    //    public List<string> Files { get; set; } = new List<string>();

    //    static StringBuilder newName;

    //    [DllImport(@"C:\Program Files\ASCON\KOMPAS-3D V16\Bin\kAPI2D5.DLL",
    //               CallingConvention =CallingConvention.StdCall,
    //               EntryPoint = "ksSetExternalFilesHandlingParam")]
    //    public static extern int ksSetExternalFilesHandlingParam(ref ExternalFilesHandlingParam par);

    //    [StructLayout(LayoutKind.Sequential)]
    //    public struct ExternalFilesHandlingParam
    //    {
    //        public CallBackExternalFilesHandling callBack; // указатель на функцию обратной связи CallBackExternalFilesNames
    //        public int allLevels;     // 1 - по всем уровням, 0 по первому уровню( не спуcкаться в подсборки )
    //        public bool docChanged;   // Признак изменения документов
    //        public ExternalFilesHandlingParam(CallBackExternalFilesHandling _callBack = null, int _allLevels = 0,bool _docChanged = false)
    //        {
    //            callBack = _callBack;
    //            allLevels = _allLevels;
    //            docChanged = _docChanged;
    //        }
    //    }

    //    public ExternalFileWrapper()
    //    {
    //        if (!ConnectToKompas())
    //            return;
    //    }

    //    public bool ConnectToKompas()
    //    {
    //        if (kompas7 != null)
    //            return true;

    //        try
    //        {
    //            kompas7 = Marshal.GetActiveObject(progId7) as IApplication;
    //        }
    //        catch (Exception e)
    //        {
    //            //logger.Write("", $"Компас не запущен : {e.Message}");
    //            return false;
    //        }

    //        if (kompas7 == null)
    //        {
    //            //logger.Write("", "Не удалось подключиться к Компас");
    //            return false;
    //        }

    //        return true;
    //    }

    //    public void Test()
    //    {
    //        IKompasDocument1 document = kompas7.ActiveDocument as IKompasDocument1;

    //        object files;
    //        object types;

    //        CallBackExternalFilesHandling cb = new CallBackExternalFilesHandling(BackExternalFilesHandling);      
    //        ExternalFilesHandlingParam param = new ExternalFilesHandlingParam(cb);

    //        ksSetExternalFilesHandlingParam(ref param);
    //        document.GetExternalFilesNamesEx(false, out files, out types);

    //        foreach (var item in (files as string[]))
    //        {
    //            Files.Add(item);
    //        }
    //    }

    //    public string BackExternalFilesHandling(int externalFileType, string externalFileName)
    //    {
    //        string from = externalFileName;
    //        newName = null;
    //        string fileName = from.Substring(from.LastIndexOf("\\") + 1);
    //        newName = new StringBuilder($"{to}\\{fileName}");
    //        File.Copy(from, newName.ToString(), true);

    //        return @"C:\Users\qdimk\Desktop\Комплектование\1.a3d";
    //    }
    //}

    public class ExternalFileWrapper2
    {
        private IApplication kompas7;
        private string progId7 = "KOMPAS.Application.7";
        private static string to = @"C:\Users\qdimk\Desktop\Комплектование";
        public List<string> Files { get; set; } = new List<string>();

        static StringBuilder newName;

        public ExternalFileWrapper2()
        {
            if (!ConnectToKompas())
                return;
        }

        public bool ConnectToKompas()
        {
            if (kompas7 != null)
                return true;

            try
            {
                kompas7 = Marshal.GetActiveObject(progId7) as IApplication;
            }
            catch (Exception e)
            {
                //logger.Write("", $"Компас не запущен : {e.Message}");
                return false;
            }

            if (kompas7 == null)
            {
                //logger.Write("", "Не удалось подключиться к Компас");
                return false;
            }

            return true;
        }

        public void Test()
        {
            IKompasDocument1 document = kompas7.ActiveDocument as IKompasDocument1;

            object files;
            object types;

            IntPtr hInst = NativeMethods.LoadLibrary(@"C:\Program Files\ASCON\KOMPAS-3D V16\Bin\kAPI2D5.DLL");
            IntPtr pFunc = NativeMethods.GetProcAddress(hInst, "ksSetExternalFilesHandlingParam");
            Marshal.GetLastWin32Error();
            ksSetExternalFilesHandlingParam setFileLinks = (ksSetExternalFilesHandlingParam)Marshal.GetDelegateForFunctionPointer(
                                                                                        pFunc,
                                                                                        typeof(ksSetExternalFilesHandlingParam));



            CallBackExternalFilesHandling cb = new CallBackExternalFilesHandling(BackExternalFilesHandling);
            ExternalFilesHandlingParam param = new ExternalFilesHandlingParam(cb);

            setFileLinks(param);

            //ksSetExternalFilesHandlingParam(ref param);
            document.GetExternalFilesNamesEx(false, out files, out types);

            foreach (var item in (files as string[]))
            {
                Files.Add(item);
            }
        }

        public string BackExternalFilesHandling(int externalFileType, string externalFileName)
        {
            string from = externalFileName;
            newName = null;
            string fileName = from.Substring(from.LastIndexOf("\\") + 1);
            newName = new StringBuilder($"{to}\\{fileName}");
            File.Copy(from, newName.ToString(), true);

            return newName.ToString();
        }
    }
}

