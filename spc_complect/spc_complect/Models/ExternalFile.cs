﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spc_complect.Models
{
    public class ExternalFile
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public string Extention { get; set; }
        public string OldPath { get; set; }
        public string NewPath { get; set; }
    }
}
