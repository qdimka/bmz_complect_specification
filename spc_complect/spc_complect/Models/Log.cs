﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spc_complect.Models
{
    public class Log
    {
        public delegate void LogTextAppendHandler(object sender, LogTextAppendEventsArgs e);
        public event LogTextAppendHandler LogTextAppend;

        private static readonly Log instance = new Log();
        private List<Error> log;

        struct Error
        {
            public string Object { get; set; }
            public string Message { get; set; }
            public string Time { get; set; }
        }

        private Log()
        {
            log = new List<Error>();
        }

        public static Log GetInstance()
        {
            return instance;
        }

        public void Save(string path)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(path))
            {
                foreach (var l in log)
                {
                    file.WriteLine($"{l.Time} \t {l.Object} \t {l.Message}");
                }
            }
        }

        public void Write(string o = "", string m = "")
        {
            string time = DateTime.Now.ToString();

            log.Add(new Error
            {
                Object = o,
                Message = m,
                Time = time
            });

            OnAppendText(new LogTextAppendEventsArgs
            {
                Text = $"{time} {o} {m}",
            });
        }

        protected virtual void OnAppendText(LogTextAppendEventsArgs e)
        {
            LogTextAppend?.Invoke(this, e);
        }
    }
}
