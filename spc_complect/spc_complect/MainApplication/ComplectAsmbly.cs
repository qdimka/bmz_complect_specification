﻿using Kompas6API5;
using Kompas6Constants3D;
using KompasAPI7;
using spc_complect.Models;
using spc_complect.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace spc_complect.MainApplication
{

    public struct Model
    {
        public const string ASSEMBLY = "A3D";
        public const string MODEL = "M3D";
    }

    public class ComplectAsmbly
    {
        public List<ExternalFile> files = new List<ExternalFile>();
        public List<string> assemblies = new List<string>();
        //private List<string> errors = new List<string>();

        private Log logger;
        private string complectDirectory;
        private string mainPath;

        private IApplication kompas7;
        private bool main = true;

        //public List<string> Errors => errors.GroupBy(p => p).Select(p => p.First()).ToList();


        public ComplectAsmbly(IApplication kompas7, string complectDirectory)
        {
            this.complectDirectory = complectDirectory;
            this.kompas7 = kompas7;
            this.logger = Log.GetInstance();
        }

        /// <summary>
        /// Обработка спецификаций
        /// </summary>
        /// <param name="file"></param>
        public void ProcessComplect(ExternalFile file)
        {
            IKompasDocument1 document = GetApiDocument(file);
            IKompasDocument3D document3D = document as IKompasDocument3D;

            if (document3D == null || document == null)
            {
                logger.Write(file.OldPath, "Файл не найден");
                file.NewPath = "-";
                return;
            }

            string newName = AssemblySaveAs(document3D/*GetApiDocument(file)*/);

            if (newName != null)
                assemblies.Add(newName);

            //Если открыта головная сборка, запишем ее
            if (main)
            {
                main = false;
                mainPath = newName;
            }

            //Добавляем внешние документы в список
            files.AddRange(GetExternalFiles(document));
            //Фильтруем
            files = files.GroupBy(p => p.Name).Select(p => p.First()).ToList();

            for (int i = 0; i < files.Count(); i++)
            {
                if (files[i].NewPath == null && files[i].NewPath != "-")
                {
                    switch (files[i].Extention)
                    {
                        case Model.ASSEMBLY:
                            if (!assemblies.Exists(p => (p.Contains(files[i].Name))))
                            {
                                ProcessComplect(files[i]);
                            }
                            break;

                        case Model.MODEL:
                            var detail = files[i];
                            detail.NewPath = FileHelpers.Copy(files[i].OldPath, complectDirectory);
                            files[i] = detail;
                            logger.Write(files[i].Name, $"скопирован в {complectDirectory}");
                            break;

                        default: break;
                    }
                }
            }
            try
            {
                document = null;
                document3D.Close(Kompas6Constants.DocumentCloseOptions.kdDoNotSaveChanges);
            }
            catch (Exception e)
            {
                logger.Write(document3D.Name, $"Не удалось закрыть документ : {e.Message}");
            }
        }

        /// <summary>
        /// Сборку сохранить как в новое место
        /// </summary>
        /// <param name="document"></param>
        public string AssemblySaveAs(IKompasDocument3D document)
        {
            try
            {
                document?.SaveAs($"{complectDirectory}\\{document?.Name}");
                logger.Write(document?.Name, $"скопирован в {complectDirectory}");
            }
            catch (Exception e)
            {
                logger.Write(document?.PathName, $"Не удалось скопировать : {e.Message}");
            }
            return $"{complectDirectory}\\{document?.Name}";
        }

        /// <summary>
        /// Подготовка сборок к изменению
        /// </summary>
        public void ProcessAssemblies()
        {
            if (assemblies == null)
                return;

            //Удалим дубликаты
            files = files.GroupBy(p => p.Name).Select(p => p.First()).ToList();

            for (int i = 0; i < files.Count; i++)
            {
                if (assemblies.Exists(p => p.Contains(files[i].Name)))
                {
                    var tmp = files[i];
                    tmp.NewPath = assemblies.Find(p => p.Contains(tmp.Name));
                    files[i] = tmp;
                }
            }

            assemblies = assemblies.Distinct().ToList();

            foreach (var item in assemblies)
            {
                ChangeReferenceInAssembly(item);
            }
        }

        /// <summary>
        /// Получить ссылку на открытый документ
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public IKompasDocument1 GetApiDocument(ExternalFile file)
        {
            IKompasDocument1 Api7Document = null;
            try
            {
                if (file != null)
                {
                    Api7Document = kompas7.Documents.Open(file.OldPath, false, false) as IKompasDocument1;
                }
                else
                {
                    Api7Document = kompas7.ActiveDocument as IKompasDocument1;
                }
            }
            catch (Exception e)
            {
                logger.Write(file.OldPath, $"Не удалось открыть документ : {e.Message}");
            }

            return Api7Document;
        }

        //public IKompasDocument3D GetApiDocument3D(ExternalFile file)
        //{

        //    IKompasDocument3D Api7Document;

        //    if (file != null)
        //    {
        //        Api7Document = kompas7.Documents.Open(file.OldPath, false, false) as IKompasDocument3D;
        //    }
        //    else
        //    {
        //        Api7Document = kompas7.ActiveDocument as IKompasDocument3D;
        //    }

        //    return Api7Document;
        //}

        /// <summary>
        /// Возвращает список внешних файлов
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public List<ExternalFile> GetExternalFiles(IKompasDocument1 document)
        {
            List<ExternalFile> tmp = new List<ExternalFile>();

            foreach (var item in document?.ExternalFilesNames[true])
            {
                string file = item as string;

                tmp.Add(new ExternalFile
                {
                    Path = file.Substring(0, file.LastIndexOf("\\")),
                    Name = GetFileName(file),
                    Extention = file.Substring(file.LastIndexOf(".") + 1).ToUpper(),
                    OldPath = file
                });
            }

            return tmp;
        }

        /// <summary>
        /// Подмена ссылок
        /// </summary>
        /// <param name="file"></param>
        public void ChangeReferenceInAssembly(string path)
        {
            IKompasDocument3D document = kompas7?.Documents.Open(path, false, false) as IKompasDocument3D;

            IPart7 part = document?.TopPart as IPart7;
            IParts7 parts = part?.Parts as IParts7;
            IPart7 current;
            try
            {
                for (int i = 0; i < parts?.Count; i++)
                {
                    current = parts?.Part[i];

                    if (current.Standard || current.IsLocal)
                        continue;

                    var file = GetFileByName(current.FileName);

                    if (file == null)
                        continue;

                    if (File.Exists(file.NewPath) && (parts.Part[i].ReadOnly != ksPartAccessTypeEnum.ksATReadOnly))
                    {
                        int counter = 0;

                        while (parts.Part[i].FileName.ToLower() != file.NewPath.ToLower() && counter < 1000)
                        {
                            parts.Part[i].FileName = file.NewPath;
                            parts.Part[i].Update();
                            counter++;
                        }
                    }


                    if (parts.Part[i].FileName != file.NewPath && parts.Part[i].FileName.Contains(file.Path))
                    {
                        logger.Write(parts.Part[i].FileName, "Не удалось изменить ссылку на файл");
                    }
                }

                document.RebuildDocument();
                //CheckLinks(document as IKompasDocument1);
                document.SaveAs(path);
                document.Close(Kompas6Constants.DocumentCloseOptions.kdDoNotSaveChanges);

                part = null;
                parts = null;
                document = null;              
            }
            catch(Exception e)
            {
                logger.Write("", $"Возникло исключение : {e.Message}");
            }
        }

        public void CheckLinks(IKompasDocument1 document)
        {
            var links = document.ExternalFilesNames[true];

            var errorLinks = (links as string[]).ToList().FindAll(p => !p.Contains(complectDirectory));

            if(errorLinks?.Count != 0)
            {
                foreach (var item in errorLinks)
                {
                    logger.Write(item, "Не удалось изменить ссылку на файл");
                }
            }
        }

        /// <summary>
        /// Возвращает имя файла из пути
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string GetFileName(string path)
        {
            return path.Substring(path.LastIndexOf("\\") + 1);
        }

        /// <summary>
        /// Поиск файла в списке по пути
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public ExternalFile GetFileByName(string path)
        {
            return files.Find(p => p.Name == GetFileName(path));
        }

        public void ReOpenNewAssembly()
        {
            if (mainPath == null)
                return;

            kompas7.Documents.Open(mainPath, true, false).Active = true;
        }
    }
}
