﻿using System;
using System.Collections.Generic;
using KompasAPI7;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Forms;
using spc_complect.Models;
using spc_complect.Utils;

namespace spc_complect.MainApplication
{
    public class ComplectSpc
    {
        public delegate void DocumentMoveHandler(object sender, DocumentEventArgs e);
        public event DocumentMoveHandler DocumentMove;

        private List<string> previousDocuments = new List<string>();

        private const string SPW = ".spw";
        private const string M3D = ".m3d";
        private const string A3D = ".a3d";

        //папка для комплектования
        private string complectDirectory;
        private IApplication kompas7;

        public ComplectSpc(IApplication kompas7,string complectDirectory)
        {
            this.complectDirectory = complectDirectory;
            this.kompas7 = kompas7;
        }

        public void RecursiveComplect(string filePath)
        {
            //Отключаем сообщения Компас
            kompas7.HideMessage = Kompas6Constants.ksHideMessageEnum.ksHideMessageNo;

            List<Attach> specifications = new List<Attach>();
            List<Attach> documents = new List<Attach>();

            ISpecificationDocument currApi7Document;
            ISpecificationDescriptions currApi7Descriptions;
            ISpecificationBaseObjects currApi7BaseObjects;
            ISpecificationObject currApi7Object;
            IAttachedDocument currApi7AttachDocument;

            if (filePath != null)
            {
                currApi7Document = kompas7.Documents.Open(filePath, false, false) as ISpecificationDocument;
            }
            else
            {
                currApi7Document = kompas7.ActiveDocument as ISpecificationDocument;
            }

            currApi7Descriptions = currApi7Document.SpecificationDescriptions as ISpecificationDescriptions;
            currApi7BaseObjects = currApi7Descriptions.Active.BaseObjects as ISpecificationBaseObjects;

            for (int i = 0; i < currApi7BaseObjects.Count; i++)
            {
                currApi7Object = currApi7BaseObjects[i] as ISpecificationBaseObject;

                if (currApi7Object == null)
                    return;

                int documentsCount = -1;

                if (currApi7Object.AttachedDocuments != null)
                {
                    documentsCount = currApi7Object.AttachedDocuments.Count;
                    //Находим документы объекта
                    for (int j = 0; j < documentsCount; j++)
                    {
                        currApi7AttachDocument = currApi7Object.AttachedDocuments[j];

                        OnDocumentMove(new DocumentEventArgs() { Name = currApi7AttachDocument.Name });

                        //Запоминаем спецификацию
                        if (currApi7AttachDocument.Name.Contains(SPW))
                            specifications.Add(new Attach()
                            {
                                Name = currApi7AttachDocument.Name,
                                Transmit = currApi7AttachDocument.Transmit
                            });

                        documents.Add(new Attach() {
                            Name = currApi7AttachDocument.Name,
                            Transmit = currApi7AttachDocument.Transmit
                        });
                    }

                    //удаляем все документы
                    while (currApi7Object.AttachedDocuments.Count != 0)
                    {
                        currApi7Object.AttachedDocuments[0].Delete();
                    }

                    //подкрепляем новые и копируем по выбранному пути.
                    foreach (var item in documents)
                    {
                        currApi7Object.AttachedDocuments.Add(FileHelpers.Copy(item.Name,
                                                         complectDirectory),
                                                         item.Transmit);
                    }

                    //обновить
                    currApi7Object.Update();
                }
                documents.Clear();
            }

            currApi7Document.Save();
            FileHelpers.Copy(currApi7Document.PathName, complectDirectory);

            //обходим вложенные спецификации
            if(specifications.Count != 0)
            {
                foreach (var specification in specifications)
                {
                    RecursiveComplect(specification.Name);
                }
                specifications.Clear();
            }

            specifications = null;
            documents = null;

            currApi7Document = null;
            currApi7Descriptions = null;
            currApi7BaseObjects = null;
            currApi7Object = null;
            currApi7AttachDocument = null;

            kompas7.HideMessage = Kompas6Constants.ksHideMessageEnum.ksHideMessageYes;
        }

        protected virtual void OnDocumentMove(DocumentEventArgs e)
        {
            DocumentMove?.Invoke(this, e);
        }
    }
}
