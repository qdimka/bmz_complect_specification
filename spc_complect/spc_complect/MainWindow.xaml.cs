﻿using KompasAPI7;
using MahApps.Metro.Controls;
using spc_complect.MainApplication;
using spc_complect.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;

namespace spc_complect
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private FolderBrowserDialog dialog;
        private string complectPath;
        private Log logger;
        private Thread complectThread;

        #region Api7
        private IApplication kompas7;
        private string progId7 = "KOMPAS.Application.7";
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            dialog = new FolderBrowserDialog();
            this.logger = Log.GetInstance();

            logger.LogTextAppend += (s, o) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    list.Items.Add($"{o.Text}");
                });
            };
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (complectThread != null && complectThread.IsAlive)
                {
                    complectThread.Abort();
                    logger.Write("", "Комплектование прервано");
                }
            }
            catch
            {

            }
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            if (complectThread != null && complectThread.IsAlive)
                return;

            try
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "Текстовые документы (*.txt)|*.txt|All files (*.*)|*.*";

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    logger.Save(dialog.FileName);
            }
            catch
            {

            }
        }

        private void open_Click(object sender, RoutedEventArgs e)
        {
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            if (Directory.Exists(dialog.SelectedPath))
            {
                complectPath = dialog.SelectedPath;
            }
            else
            {
                complectPath = null;
            }

            complectDirectory.Text = complectPath;
        }

        private void complect_Click(object sender, RoutedEventArgs e)
        {
            if (complectThread != null && complectThread.IsAlive)
                return;

            if (!ConnectToKompas())
                return;
            try
            {
                if (kompas7.ActiveDocument is ISpecificationDocument)
                    ProcessSpc();

                if (kompas7.ActiveDocument is IKompasDocument3D)
                    ProcessAsmbly();
            }
            catch (Exception ex)
            {
                logger.Write($"{ex.Message}");
            }
        }

        public bool ConnectToKompas()
        {
            if (kompas7 != null)
                return true;

            try
            {
                kompas7 = Marshal.GetActiveObject(progId7) as IApplication;
            }
            catch (Exception e)
            {
                logger.Write($"Компас не запущен : {e.Message}");
                return false;
            }

            if (kompas7 == null)
            {
                logger.Write("Не удалось подключиться к Компас");
                return false;
            }

            return true;
        }

        public void ProcessSpc()
        {
            //if (complectPath == null)
            //{
            //    System.Windows.Forms.MessageBox.Show("Не выбран путь", "Ошибка");
            //    return;
            //}

            //ComplectSpc complect = new ComplectSpc(kompas7,dialog.SelectedPath);

            //complect.DocumentMove += (s, o) =>
            //{
            //    this.Dispatcher.Invoke(() =>
            //    {
            //        list.Items.Add($"{o.Name.Substring(o.Name.LastIndexOf("\\") + 1)} -> { complectPath }");
            //    });
            //};

            //new Thread(() =>
            //{
            //    complect.RecursiveComplect(null);
            //    System.Windows.Forms.MessageBox.Show("Комплектование завершено", "Комлектовщик");
            //})
            //.Start();
        }

        public void ProcessAsmbly()
        {
            if (complectThread!= null && complectThread.IsAlive)
            {
                logger.Write("Комплектование уже запущено");
                return;
            }

            if (complectPath == null)
            {
                logger.Write("Не указан путь к папке Комплектования");
                return;
            }

            ComplectAsmbly complect = new ComplectAsmbly(kompas7, complectPath);

            complectThread = new Thread(() =>
            {
                logger.Write("Комплектование запущено");

                complect.ProcessComplect(null);

                complect.ProcessAssemblies();

                complect.ReOpenNewAssembly();

                logger.Write("Комплектование завершено");

                Marshal.ReleaseComObject(kompas7);
            });

            complectThread.Start();
        }

        private void complectDirectory_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(complectDirectory.Text))
            {
                complectPath = complectDirectory.Text;
            }
            else
            {
                complectPath = null;
                logger.Write("Указанной папки не существует");
            }
        }
    }
}
