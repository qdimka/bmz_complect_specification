﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spc_complect.Utils
{
    public static class FileHelpers
    {
        public static string Copy(string from, string to)
        {
            string fileName = from.Substring(from.LastIndexOf("\\") + 1);

            string newPath = $"{to}\\{fileName}";

            if (File.Exists(newPath))
                return newPath;

            try
            {
                File.Copy(from, newPath, true);
            }
            catch (IOException ex)
            {

            }
            return newPath;
        }
    }
}
